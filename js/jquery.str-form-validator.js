(
    function($)
    {
        var regExMail  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-]{2,})+\.)+([a-zA-Z0-9]{2,})+$/,
            regExNum   = /^[0-9 ]+$/,
            okBadge     = '<span class="badge">Compilato</span>',
            alertBadge  = '<span class="badge">Da rivedere</span>',
            errorBadge  = '<span class="badge">Da compilare</span>';

        $.fn.strContValidator = function()
        {
            return this.each(
                function()
                {
                    /*
                        Functions setup
                        --------------------------------------------------------------------
                        Before execution, a little bit of setup to warm up...
                    */

                    // Moving out the label
                    // ---------------------------------------------------------------------
                    // Here the '__moved' class is given to enable an animation.
                    function moveOutLabel(m)
                    {
                        m.siblings('.contact-form--label__movable')
                         .addClass('contact-form--label__moved');

                        m.siblings('.contact-form--label')
                         .children('.fas')
                         .fadeOut(150);

                        m.siblings('.contact-form--label')
                         .children('.badge')
                         .fadeOut(150)
                         .queue(
                            function()
                            {
                                $(this).delay(151)
                                       .remove();
                                }
                            );
                    }

                    // Check attribution
                    // ---------------------------------------------------------------------
                    // Here attribution of the check mark.

                    // For input fields
                    // ---------------------------------------------------------------------
                    function okFlag(f)
                    {
                        f.siblings('.contact-form--label')
                         .children('.badge')
                         .remove();

                        f.siblings('.contact-form--label')
                         .children('.fas')
                         .removeClass('fa-pencil-alt fa-check fa-exclamation-triangle fa-info-circle')
                         .addClass('fa-check')
                         .fadeIn(150)
                         .after(' ' + okBadge);
                    }

                    // For for privacy
                    // ---------------------------------------------------------------------
                    function okFlagP(fp)
                    {
                        fp.siblings('.fas, .badge')
                          .remove();
                        fp.siblings('span')
                          .after('<i class="fas fa-check"></i>' + ' ' + okBadge);
                    }

                    // Error attribution
                    // ---------------------------------------------------------------------
                    // Here attribution of the error mark.

                    // For input fields
                    // ---------------------------------------------------------------------
                    function errorFlag(f)
                    {
                        f.siblings('.contact-form--label')
                         .children('.badge')
                         .remove();

                        f.siblings('.contact-form--label')
                         .children('.fas')
                         .removeClass('fa-pencil-alt fa-check fa-exclamation-triangle fa-info-circle')
                         .addClass('fa-exclamation-triangle')
                         .fadeIn(150)
                         .after(' ' + errorBadge);
                    }

                    // For for privacy
                    // ---------------------------------------------------------------------
                    function errorFlagP(fp)
                    {
                        fp.siblings('.fas, .badge')
                               .remove();
                        fp.siblings('span')
                               .after('<i class="fas fa-exclamation-triangle"></i>' + ' ' + errorBadge);
                    }

                    // Alert attribution
                    // ---------------------------------------------------------------------
                    // Here attribution of the alert mark.

                    // For input fields
                    // ---------------------------------------------------------------------
                    function alertFlag(f)
                    {
                        f.siblings('.contact-form--label')
                         .children('.badge')
                         .remove();

                        f.siblings('.contact-form--label')
                         .children('.fas')
                         .removeClass('fa-pencil-alt fa-check fa-exclamation-triangle fa-info-circle')
                         .addClass('fa-info-circle')
                         .fadeIn(150)
                         .after(' ' + alertBadge);
                    }

                    // For for privacy
                    // ---------------------------------------------------------------------
                    function alertFlagP(fp)
                    {
                        fp.siblings('.fas, .badge')
                               .remove();
                        fp.siblings('span')
                               .after('<i class="fas fa-info-circle"></i>' + ' ' + alertBadge);
                    }


                    /*
                        Test of content Inputs
                        ----------------------------------------------------------------
                        This function determines if the input and textarea fields are
                        full or empty AND if the content is compliant to what is needed,
                        setting a 'yes' or 'nope' flag to their content.
                    */
                    function testContInputs(l)
                    {
                        // If empty return to base status...
                        if (l.val() === '' || l.val() === null || l.val() === undefined)
                        {
                            // for security reset 'sendReady' status
                            l.data('sendReady', 'nope');
                        }
                        // If not empty, check the contents
                        else if (l.val() !== '' || l.val() !== null || l.val() !== undefined)
                        {
                            // In case of name input and message textarea...
                            if (l.attr('type') === 'text' || l.attr('id') === formID.substr(1) + '--message')
                            {
                                // ...No special controls, just set to ok
                                l.data('sendReady', 'ok');
                            }

                            // In case of captcha, it checks if the content
                            // matches an numbers-only regex.
                            if (l.attr('id') === formID.substr(1) + '--cap')
                            {
                                if (regExNum.test(l.val()) === true)
                                {
                                    l.data('sendReady', 'ok');
                                }

                                else
                                {
                                    l.data('sendReady', 'nope');
                                }
                            }

                            // In case of telephone number, it checks if the content
                            // matches an numbers-only regex
                            if (l.attr('type') === 'tel')
                            {
                                if (regExNum.test(l.val()) === true)
                                {
                                    l.data('sendReady', 'ok');
                                }

                                else
                                {
                                    l.data('sendReady', 'nope');
                                }
                            }

                            // In case of telephone number, it checks if the content
                            // matches an email regex.
                            if (l.attr('type') === 'email')
                            {
                                if (regExMail.test(l.val()) === true)
                                {
                                    l.data('sendReady', 'ok');
                                }

                                else
                                {
                                    l.data('sendReady', 'nope');
                                }
                            }
                        }
                    }

                    /*
                        Privacy input checking
                        ---------------------------------------------------------------------
                        Here the privacy input is checked to see if it is ready to send or
                        no.
                    */
                    function testPrivaCheck(l) {
                        if (l.prop('checked'))
                        {
                            l.data('sendReady', 'ok');
                        }
                        else
                        {
                            l.data('sendReady', 'nope');
                        }
                    }


                    /*
                        Strike data ready test
                        --------------------------------------------------------------------
                        This test fires the previously set verifications on each element
                        of the form.
                        If the checked element is negative, it's pushed in an array then,
                        the array is checked; if the array has elements, the function
                        returns a 'nope', used for verification.
                    */

                    function strikeDataReady(t, p)
                    {
                        var nopeArr = [];

                        t.each(
                            function()
                            {
                                testContInputs($(this));
                                if ($(this).data('sendReady') === 'nope')
                                {
                                    nopeArr.push($(this).attr('id'));
                                }
                            }
                        );

                        p.each(
                            function()
                            {
                                testPrivaCheck($(this));
                                if ($(this).data('sendReady') === 'nope')
                                {
                                    nopeArr.push($(this).attr('id'));
                                }
                            }
                        );

                        if (nopeArr.length > 0)
                        {
                            return 'nope';
                        }
                    }


                    /*
                        Contact Form Executor!
                        --------------------------------------------------------------------
                        To ensure less clutter, the code is executed only if the form is
                        found in the page.
                        Based on the class which identifies the contact forms, the attribute
                        ID is found and used
                    */

                    if ($('.contact-form').length > 0)
                    {
                        // Basic setup operations if the form is found
                        // ----------------------------------------------------------------------------

                        var formID       = '#' + $(this).attr('id'),
                            txtFields    = $(formID + ' ' + '.contact-form--input-text, ' + formID + ' ' + '.contact-form--textarea'),
                            privElement  = $(formID + ' ' + formID + '--check-privacy');

                        // First test for form elements
                        strikeDataReady(txtFields, privElement);

                        // Testing each of the elements on focus in and out to flag them, wita a sendReady data and a
                        // visual flag...
                        txtFields.each(
                            function()
                            {
                                $(this).focusin(
                                    function()
                                    {
                                        moveOutLabel($(this));
                                    }
                                ).focusout(
                                    function()
                                    {
                                        testContInputs($(this));

                                        if ($(this).data('sendReady') === 'ok')
                                        {
                                            okFlag($(this));
                                        }
                                        else
                                        {
                                            alertFlag($(this));
                                        }
                                    }
                                );
                            }
                        );

                        // ... doing the same for privacy check
                        privElement.change(
                            function()
                            {
                                if ($(this).prop('checked'))
                                {
                                    okFlagP($(this));
                                }
                                else{
                                    alertFlagP($(this));
                                }
                            }
                        );

                        // Submit operations
                        // ----------------------------------------------------------------------------
                        $(formID).submit(
                            function(s)
                            {
                                // If the checks return a 'nope' status, errors are reported
                                // to the user
                                if (strikeDataReady(txtFields, privElement) === 'nope')
                                {
                                    txtFields.each(
                                        function()
                                        {
                                            if ($(this).data('sendReady') === 'nope')
                                            {
                                                errorFlag($(this));
                                                s.preventDefault();
                                                // Prevent sending for IE
                                                s.returnValue = false;
                                            }
                                        }
                                    );

                                    privElement.each(
                                        function()
                                        {
                                            if ($(this).data('sendReady') === 'nope')
                                            {
                                                errorFlagP($(this));
                                                s.preventDefault();
                                                // Prevent sending for IE
                                                s.returnValue = false;
                                            }
                                        }
                                    );
                                }
                                // ...else, if the form is error-free, it is sent...
                                else
                                {
                                    //s.preventDefault(); /* Use to test the waiting box */

                                    // ...showing a message after a small timeout

                                    /*jshint multistr: true */
                                    var waitingBox     = '<div class="contact-form--bg-fader">\
                                        <div class="contact-form--load-message">\
                                        <i class="fas fa-paper-plane"></i>\
                                        <p>\
                                        Messaggio in partenza, attendi qualche secondo.\
                                        <i class="fas fa-cog fa-spin"></i>\
                                        </p>\
                                        </div>\
                                        <div class="close-button">\
                                        <i class="fas fa-times"></i>\
                                        </div>\
                                        </div>';
                                    $(formID).delay(1500)
                                         .queue(
                                        function()
                                        {
                                            $(this).after($(waitingBox).fadeIn(500))
                                                     .dequeue();
                                            $('.contact-form--bg-fader, .contact-form--bg-fader .fa-close').click(
                                            function()
                                                {
                                                    $('.contact-form--bg-fader').remove();
                                                }
                                            );
                                        }
                                    );
                                }
                            }
                        );

                        // Error handling operations
                        // ----------------------------------------------------------------------------
                        // If the form sending results in an error, a field is created
                        // and the verification are re-issued to setup the form again.
                        if ($(formID + '-result.contact-form--anchor-point__error').length > 0)
                        {

                            strikeDataReady(txtFields, privElement);

                            txtFields.each(
                                function()
                                {
                                    testContInputs($(this));

                                    if ($(this).val() !== '' || $(this).val() !== null || $(this).val() !== undefined)
                                    {
                                        if ($(this).data('sendReady') === 'ok')
                                        {
                                            moveOutLabel($(this));
                                            okFlag($(this));
                                        }
                                        else
                                        {
                                            alertFlag($(this));
                                        }
                                    }
                                }
                            );

                            privElement.each(
                                function()
                                {
                                    testPrivaCheck($(this));

                                    if ($(this).data('sendReady') === 'ok')
                                    {
                                        okFlagP($(this));
                                    }
                                    else
                                    {
                                        alertFlagP($(this));
                                    }
                                }
                            );
                        }
                    }
                }
            );
        };

    }
    (jQuery)
);
