<?php
	/*
		Form and session data setup
		------------------------------------------------------------------------

	*/
	// Captcha numbers

	// Getting random numbers and set a variable to use them
	$captcha_numbers = st_CaptchaNumber();
	// Based on the exact variable extract the three numbers
	// The numbers must be paired with a single function
	$rand_lnum_1 = $captcha_numbers['lnum_1'];
	$rand_lnum_2 = $captcha_numbers['lnum_2'];
	$rand_nsum   = $captcha_numbers['nsum'];

	// Store the paramenters to be used during the send action
	$_SESSION['pagevar']['path']                = path();
	$_SESSION['pagevar']['url']                 = url();
	$_SESSION['pagevar']['site_host']           = st_Data('site', 'host');
	$_SESSION['pagevar']['owner']               = st_Data('site', 'name');
	$_SESSION['pagevar']['resp_mail']           = st_Data('owner', 'email_1');
	$_SESSION['pagevar']['nsum']                = $rand_nsum;
	$_SESSION['pagevar']['form_ID']             = $form_ID;
	$_SESSION['pagevar']['sendmail_recipient']  = st_Data('owner', 'sendmail_recipient');
	$_SESSION['pagevar']['sendmail_pass']       = st_Data('owner', 'sendmail_pass');


	if (
		   isset($_SESSION['err']) && !empty($_SESSION['err'])
		|| isset($_SESSION['send_error']) && !empty($_SESSION['send_error'])
		|| isset($_SESSION['send_confirm']) && !empty($_SESSION['send_confirm'])
	)
	{
		if (isset($_SESSION['err']) && !empty($_SESSION['err']) || isset($_SESSION['send_error']) && !empty($_SESSION['send_error']))
		{
			$result_class = '__error';
		}
		if (isset($_SESSION['send_confirm']) && !empty($_SESSION['send_confirm']))
		{
			$result_class = '__success';
		}
		echo "<div class=\"contact-form--anchor-point contact-form--anchor-point{$result_class}\" id=\"{$form_ID}-result\"></div>";
	}

	if (isset($_SESSION['err']) && !empty($_SESSION['err']))
	{
		echo '<div class="send-message-log alert alert-danger" role="alert">';
		echo '<span class="fas fa-exclamation-triangle"></span> C&#8217;è qualche errore';
		echo '<hr>';
		echo '<ul>';
		foreach ($_SESSION['err'] as $error_mess)
		{
			echo "<li>{$error_mess}</li>";
		}
		echo '</ul>';
		echo '</div>';

		echo '<div class="send-message-log alert alert-warning" role="alert">';
		echo '<span class="fas fa-info-circle"></span> Dovrai reinserire il <strong>numero di verifica</strong>';
		if (isset($_SESSION['data_store_mail']['privacy']) && !empty($_SESSION['data_store_mail']['privacy']))
		{
			echo ' e il <strong>consenso privacy</strong>';
		}
		echo '.';
		echo '</div>';
	}

	if (isset($_SESSION['send_error']) && !empty($_SESSION['send_error']))
	{
		echo '<div class="send-message-log alert alert-danger" role="alert">';
		echo "<span class=\"fas fa-exclamation-triangle\"></span> {$_SESSION['send_error']}";
		echo '</div>';
	}

	if (isset($_SESSION['send_confirm']) && !empty($_SESSION['send_confirm']))
	{
		echo '<div class="send-message-log alert alert-success" role="alert">';
		echo "<span class=\"fas fa-thumbs-up\"></span> {$_SESSION['send_confirm']}";
		echo '</div>';
	}

	$message = isset($_SESSION['data_store_mail']['message']) ? $_SESSION['data_store_mail']['message'] : '';
	$name    = isset($_SESSION['data_store_mail']['name'])    ? $_SESSION['data_store_mail']['name']    : '';
	$tel     = isset($_SESSION['data_store_mail']['tel'])     ? $_SESSION['data_store_mail']['tel']     : '';
	$email   = isset($_SESSION['data_store_mail']['email'])   ? $_SESSION['data_store_mail']['email']   : '';
	$base_badge   = '<span class="fas fa-pencil-alt"></span>';
	$ok_badge     = '<span class="fas fa-check"></span><span class="badge">Compilato</span>';
	$error_badge  = '<span class="fas fa-exclamation-triangle"></span><span class="badge">Da compilare</span>';
	$alert_badge  = '<span class="fas fa-info-circle"></span><span class="badge">Da rivedere</span>';
?>
<form class="contact-form contact-form--basic-message" id="<?php echo $form_ID ?>" action="<?php e_url('strike-scripts/scr-send-contact.php') ?>" method="post">

	<div class="contact-form--panel">

		<h3 class="contact-form--panel-title">Step 1: su di te</h3>

		<fieldset class="contact-form--fieldset field--message">

			<h4 class="contact-form--field-title">Il tuo messaggio</h4>

			<div class="contact-form--rich-textfield">

				<label class="contact-form--label" for="<?php echo $form_ID . '--message' ?>">
					<?php
						echo 'Di cosa vuoi parlarci?' . ' ';

						if (isset($_SESSION['err']['message']) && !empty($_SESSION['err']['message']))
						{
							echo $error_badge;
						}
						else if (isset($_SESSION['data_store_mail']['message']) && !empty($_SESSION['data_store_mail']['message']))
						{
							echo $ok_badge;
						}
						else
						{
							echo $base_badge;
						}
					?>
				</label>
				<textarea class="contact-form--textarea" id="<?php echo $form_ID . '--message' ?>" name="<?php echo $form_ID . '--message' ?>" rows="5" cols="200"></textarea>

			</div>

		</fieldset>

		<fieldset class="contact-form--fieldset field--info">

			<h4 class="contact-form--field-title">I dati per ricontattarti</h4>

			<div class="contact-form--rich-inputfields-group">

				<div class="contact-form--rich-inputfield">

					<label class="contact-form--label contact-form--label__movable" for="<?php echo $form_ID . '--name' ?>">
						<?php
							echo 'Nome' . ' ';

							if (isset($_SESSION['err']['name']) && !empty($_SESSION['err']['name']))
							{
								echo $error_badge;
							}
							else if (isset($_SESSION['data_store_mail']['name']) && !empty($_SESSION['data_store_mail']['name']))
							{
								echo $ok_badge;
							}
							else
							{
								echo $base_badge;
							}
						?>
					</label>
					<input class="contact-form--input-text" id="<?php echo $form_ID . '--name' ?>" name="<?php echo $form_ID . '--name' ?>" type="text" placeholder="Il tuo nome">

				</div>

				<div class="contact-form--rich-inputfield">

					<label class="contact-form--label contact-form--label__movable" for="<?php echo $form_ID . '--tel' ?>">
						<?php
							echo 'Telefono' . ' ';

							if (isset($_SESSION['err']['tel']) && !empty($_SESSION['err']['tel']))
							{
								echo $error_badge;
							}
							else if (isset($_SESSION['data_store_mail']['tel']) && !empty($_SESSION['data_store_mail']['tel']))
							{
								echo $ok_badge;
							}
							else
							{
								echo $base_badge;
							}
						?>
					</label>
					<input class="contact-form--input-text" id="<?php echo $form_ID . '--tel' ?>" name="<?php echo $form_ID . '--tel' ?>" type="tel" placeholder="Il tuo numero di telefono">

				</div>

				<div class="contact-form--rich-inputfield">

					<label class="contact-form--label contact-form--label__movable" for="<?php echo $form_ID . '--email' ?>">
						<?php
							echo 'Indirizzo email' . ' ';

							if (isset($_SESSION['err']['email']) && !empty($_SESSION['err']['email']))
							{
								echo $error_badge;
							}
							else if (isset($_SESSION['data_store_mail']['email']) && !empty($_SESSION['data_store_mail']['email']))
							{
								echo $ok_badge;
							}
							else
							{
								echo $base_badge;
							}
						?>
					</label>
					<input class="contact-form--input-text" id="<?php echo $form_ID . '--email' ?>" name="<?php echo $form_ID . '--email' ?>" type="email" placeholder="Il tuo indirizzo email">

				</div>

			</div>

		</fieldset>

	</div>

	<div class="contact-form--panel">

		<h3 class="contact-form--panel-title">Step 2: consensi e verifica</h3>

		<fieldset class="contact-form--fieldset field--privacy">

			<h4 class="contact-form--field-title">Privacy</h4>

			<p class="contact-form--infobox">
				<span class="fas fa-info-circle"></span> I tuoi dati verranno usati <strong>solo allo scopo di ricontattarti</strong>.
			</p>

			<label class="" for="<?php echo $form_ID . '--check-privacy' ?>">

				<input id="<?php echo $form_ID . '--check-privacy' ?>" name="<?php echo $form_ID . '--check-privacy' ?>" type="checkbox" form="<?php echo $form_ID ?>">
				<span>
					Inviando il presente modulo di contatto acconsento al trattamento dei miei dati personali. <a href="<?php e_url('privacy.php') ?>">Privacy</a>.
				</span>
				<?php
					if (isset($_SESSION['err']['privacy']) && !empty($_SESSION['err']['privacy']))
					{
						echo $error_badge;
					}
					else if (isset($_SESSION['data_store_mail']['privacy']) && !empty($_SESSION['data_store_mail']['privacy']))
					{
						echo $alert_badge;
					}
				?>

			</label>

		</fieldset>

		<fieldset class="contact-form--fieldset field--verification">

			<h4 class="contact-form--field-title">Verifica</h4>

			<p class="contact-form--infobox">
				<span class="fas fa-info-circle"></span> Riporta <strong><em>in numero</em></strong> il risultato della somma.
				<br>
				<span class="fas fa-info-circle"></span> <em>I numeri sono casuali e cambiano ogni volta che visiti la pagina.</em>
			</p>

			<div class="contact-form--rich-inputfield">

				<label class="contact-form--label contact-form--label__movable" for="<?php echo $form_ID . '--cap' ?>">
					<?php
						echo $rand_lnum_1 . ' + ' . $rand_lnum_2 . ' ';
						if (isset($_SESSION['err']) && !empty($_SESSION['err']))
						{
							echo $alert_badge;
						}
						else if (isset($_SESSION['send_error']) && !empty($_SESSION['send_error']))
						{
							echo $error_badge;
						}
						else
						{
							echo $base_badge;
						}
					?>
				</label>
				<input class="contact-form--input-text" id="<?php echo $form_ID . '--cap' ?>" name="<?php echo $form_ID . '--cap' ?>" type="text" form="<?php echo $form_ID ?>" placeholder="Qui la somma in numero">

			</div>

		</fieldset>

		<fieldset>

			<h3 class="contact-form--panel-title">Step 3: invio</h3>

			<button class="button send-button" id="<?php echo $form_ID . '--send' ?>" type="submit" name="<?php echo $form_ID . '--send' ?>" form="<?php echo $form_ID ?>">
				Invia il messaggio <span class="fas fa-paper-plane"></span>
			</button>

		</fieldset>

	</div>

	<?php
		if (isset($_SESSION['err']) && !empty($_SESSION['err']))
		{
			unset($_SESSION['err']);
		}

		if (isset($_SESSION['send_error']) && !empty($_SESSION['send_error']))
		{
			unset($_SESSION['send_error']);
		}

		if (isset($_SESSION['send_confirm']) && !empty($_SESSION['send_confirm']))
		{
			unset($_SESSION['send_confirm']);
		}

		if(isset($_SESSION['data_store_mail']) && !empty($_SESSION['data_store_mail']))
		{
			unset($_SESSION['data_store_mail']);
		}
	?>

</form>
